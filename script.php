
<?php

    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $lista_de_compras = require 'lista-de-compras.php';
    
    require 'funcoes.php';

    $palavras_certas = array(
        "Papel Hignico"     => "Papel Higiênico",
        "Brocolis"          => "Brócolis",
        "Chocolate ao leit" => "Chocolate ao leite",
        "Sabao em po"       => "Sabão em pó"
    );

    $output = array();

    $meses_ordenados = getsortedMonth($lista_de_compras);
    $conteudo = "Mês;Categoria;Produto;Quantidade;";

    foreach($meses_ordenados as $mes => $values)
    {
        $alimentos = $values['alimentos'];
        $higiene_pessoal = $values['higiene_pessoal'];
        $limpeza = $values['limpeza'];

        if(!empty($alimentos))
        {
            arsort($alimentos);
            
            foreach($alimentos as $produto => $quantidade)
            {
                if(!in_array($alimentos, $palavras_certas))
                {
                    $conteudo.="\n$mes;alimentos;$produto;$quantidade;";
                }
                else
                {
                    $conteudo.="\n$mes;alimentos;$palavras_certas[$produto];$quantidade;";
                }
            }
        }
        if(!empty($higiene_pessoal))
        {
            arsort($higiene_pessoal);

            foreach($higiene_pessoal as $produto => $quantidade)
            {
                if(!in_array($produto, $palavras_certas))
                {
                    $conteudo.="\n$mes;higiene pessoal;$produto;$quantidade;";
                }
                else
                {
                    $conteudo.="\n$mes;higiene pessoal;$palavras_certas[$produto];$quantidade;";
                }
            }
        }
        if(!empty($limpeza))
        {
            arsort($limpeza);

            foreach($limpeza as $produto => $quantidade)
            {
                if(!in_array($produto, $palavras_certas))
                {
                    $conteudo.="\n$mes;limpeza;$produto;$quantidade;";
                }
                else
                {
                    $conteudo.="\n$mes;limpeza;$palavras_certas[$produto];$quantidade;";
                }
            }
        }
    }

    $fp = fopen("compras-do-ano.csv", "w");
    $escreve = fwrite($fp, mb_convert_encoding($conteudo, 'UTF-16LE', 'UTF-8'));
    fclose($fp);
