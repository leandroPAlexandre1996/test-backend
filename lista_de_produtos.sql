-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 11-Jul-2021 às 06:17
-- Versão do servidor: 10.4.17-MariaDB
-- versão do PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lista_de_produtos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `lista_de_produtos`
--

CREATE TABLE `lista_de_produtos` (
  `id` int(11) NOT NULL,
  `mes` varchar(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data_de_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `data_de_atualizacao` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `lista_de_produtos`
--

INSERT INTO `lista_de_produtos` (`id`, `mes`, `categoria`, `produto`, `quantidade`, `data_de_criacao`, `data_de_atualizacao`, `status`) VALUES
(1, 'janeiro', 'alimentos', 'Pão de forma', 100, '2021-07-08 02:17:59', '2021-07-08 02:17:59', 1),
(2, 'janeiro', 'alimentos', 'Nutella', 50, '2021-07-08 02:17:59', '2021-07-08 02:17:59', 1),
(3, 'janeiro', 'alimentos', 'Arroz', 200, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(4, 'janeiro', 'alimentos', 'Feijão', 150, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(5, 'janeiro', 'limpeza', 'Veja multiuso', 50, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(6, 'janeiro', 'limpeza', 'Sabão em pó', 50, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(7, 'janeiro', 'limpeza', 'Desinfetante', 100, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(8, 'janeiro', 'higiene_pessoal', 'Creme dental', 500, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(9, 'janeiro', 'higiene_pessoal', 'Sabonete Protex', 500, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(10, 'janeiro', 'higiene_pessoal', 'Escova de dente', 500, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(11, 'janeiro', 'higiene_pessoal', 'Papel Higiênico', 1000, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(12, 'fevereiro', 'alimentos', 'Pão de forma', 50, '2021-07-08 02:18:00', '2021-07-08 02:18:00', 1),
(13, 'fevereiro', 'alimentos', 'Queijo minas', 50, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(14, 'fevereiro', 'alimentos', 'Geléria de morango', 250, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(15, 'fevereiro', 'limpeza', 'Pano de chão', 300, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(16, 'fevereiro', 'limpeza', 'Rodo', 301, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(17, 'fevereiro', 'higiene_pessoal', 'Creme dental', 500, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(18, 'fevereiro', 'higiene_pessoal', 'Sabonete Dove', 500, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(19, 'fevereiro', 'higiene_pessoal', 'Papel Higiênico', 1000, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(20, 'marco', 'alimentos', 'Ovos', 1200, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(21, 'marco', 'alimentos', 'Iogurte', 500, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(22, 'marco', 'alimentos', 'Pasta de Amendoim', 500, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(23, 'marco', 'limpeza', 'Detergente', 100, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(24, 'marco', 'limpeza', 'Sabão em pó', 100, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(25, 'marco', 'higiene_pessoal', 'Enxaguante bocal', 500, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(26, 'abril', 'alimentos', 'Doritos', 50, '2021-07-08 02:18:01', '2021-07-08 02:18:01', 1),
(27, 'abril', 'alimentos', 'Chocolate ao leite', 100, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(28, 'abril', 'alimentos', 'Filé Mignon', 350, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(29, 'abril', 'limpeza', 'Esponja de aço', 100, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(30, 'abril', 'higiene_pessoal', 'Fio dental', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(31, 'abril', 'higiene_pessoal', 'Escova de dente', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(32, 'abril', 'higiene_pessoal', 'Creme dental', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(33, 'maio', 'alimentos', 'Brócolis', 1000, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(34, 'maio', 'alimentos', 'Tomate', 1000, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(35, 'maio', 'alimentos', 'Morango', 10001, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(36, 'maio', 'alimentos', 'Berinjela', 100, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(37, 'maio', 'alimentos', 'Pepino', 100, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(38, 'maio', 'alimentos', 'Arroz integral', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(39, 'maio', 'alimentos', 'Feijão', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(40, 'maio', 'alimentos', 'Filé de frango', 500, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(41, 'maio', 'alimentos', 'Leite desnatado', 2000, '2021-07-08 02:18:02', '2021-07-08 02:18:02', 1),
(42, 'maio', 'limpeza', 'Diabo verde', 100, '2021-07-08 02:18:03', '2021-07-08 02:18:03', 1),
(43, 'maio', 'limpeza', 'MOP', 100, '2021-07-08 02:18:03', '2021-07-08 02:18:03', 1),
(44, 'maio', 'higiene_pessoal', 'Protex', 500, '2021-07-08 02:18:03', '2021-07-08 02:18:03', 1),
(45, 'maio', 'higiene_pessoal', 'Papel Higiênico', 500, '2021-07-08 02:18:03', '2021-07-08 02:18:03', 1),
(46, 'maio', 'higiene_pessoal', 'Shampoo', 500, '2021-07-08 02:18:03', '2021-07-08 02:18:03', 1),
(47, 'janeiro', 'alimentos', 'Arroz', 200, '2021-07-11 03:15:11', '2021-07-11 03:15:11', 1),
(48, 'janeiro', 'alimentos', 'Feijão', 150, '2021-07-11 03:15:11', '2021-07-11 03:15:11', 1),
(49, 'janeiro', 'alimentos', 'Pão de forma', 100, '2021-07-11 03:15:11', '2021-07-11 03:15:11', 1),
(50, 'janeiro', 'alimentos', 'Nutella', 50, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(51, 'janeiro', 'hgiene pessoal', 'Papel Hignico', 1000, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(52, 'janeiro', 'hgiene pessoal', 'Creme dental', 500, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(53, 'janeiro', 'hgiene pessoal', 'Sabonete Protex', 500, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(54, 'janeiro', 'hgiene pessoal', 'Escova de dente', 500, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(55, 'janeiro', 'limpeza', 'Desinfetante', 100, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(56, 'janeiro', 'limpeza', 'Veja multiuso', 50, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(57, 'janeiro', 'limpeza', 'Sabao em po', 50, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(58, 'fevereiro', 'alimentos', 'Geléria de morango', 250, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(59, 'fevereiro', 'alimentos', 'Pão de forma', 50, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(60, 'fevereiro', 'alimentos', 'Queijo minas', 50, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(61, 'fevereiro', 'hgiene pessoal', 'Papel Hignico', 1000, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(62, 'fevereiro', 'hgiene pessoal', 'Creme dental', 500, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(63, 'fevereiro', 'hgiene pessoal', 'Sabonete Dove', 500, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(64, 'fevereiro', 'limpeza', 'Rodo', 301, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(65, 'fevereiro', 'limpeza', 'Pano de chão', 300, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(66, 'marco', 'alimentos', 'Ovos', 1200, '2021-07-11 03:15:12', '2021-07-11 03:15:12', 1),
(67, 'marco', 'alimentos', 'Iogurte', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(68, 'marco', 'alimentos', 'Pasta de Amendoim', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(69, 'marco', 'hgiene pessoal', 'Enxaguante bocal', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(70, 'marco', 'limpeza', 'Detergente', 100, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(71, 'marco', 'limpeza', 'Sabao em po', 100, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(72, 'abril', 'alimentos', 'Filé Mignon', 350, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(73, 'abril', 'alimentos', 'Chocolate ao leit', 100, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(74, 'abril', 'alimentos', 'Doritos', 50, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(75, 'abril', 'hgiene pessoal', 'Fio dental', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(76, 'abril', 'hgiene pessoal', 'Escova de dente', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(77, 'abril', 'hgiene pessoal', 'Creme dental', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(78, 'abril', 'limpeza', 'Esponja de aço', 100, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(79, 'maio', 'alimentos', 'Morango', 10001, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(80, 'maio', 'alimentos', 'Leite desnatado', 2000, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(81, 'maio', 'alimentos', 'Brocolis', 1000, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(82, 'maio', 'alimentos', 'Tomate', 1000, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(83, 'maio', 'alimentos', 'Arroz integral', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(84, 'maio', 'alimentos', 'Feijão', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(85, 'maio', 'alimentos', 'Filé de frango', 500, '2021-07-11 03:15:13', '2021-07-11 03:15:13', 1),
(86, 'maio', 'alimentos', 'Berinjela', 100, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(87, 'maio', 'alimentos', 'Pepino', 100, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(88, 'maio', 'hgiene pessoal', 'Protex', 500, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(89, 'maio', 'hgiene pessoal', 'Papel Hignico', 500, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(90, 'maio', 'hgiene pessoal', 'Shampoo', 500, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(91, 'maio', 'limpeza', 'Diabo verde', 100, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1),
(92, 'maio', 'limpeza', 'MOP', 100, '2021-07-11 03:15:14', '2021-07-11 03:15:14', 1);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `lista_de_produtos`
--
ALTER TABLE `lista_de_produtos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `lista_de_produtos`
--
ALTER TABLE `lista_de_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
