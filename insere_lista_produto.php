<?php

    $pdo = new PDO("mysql:host=localhost;dbname=lista_de_produtos", "root", ""); 
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $lista_de_compras = require 'lista-de-compras.php';
    require 'funcoes.php';

    $palavras_certas = array(
        "Papel Hignico" => "Papel Higiênico",
        "Brocolis" => "Brócolis",
        "Chocolate ao leit" => "Chocolate ao leite",
        "Sabao em po" => "Sabão em pó"
    );

    $output = array();

    $meses_ordenados = getsortedMonth($lista_de_compras);
    $conteudo = "Mês;Categoria;Produto;Quantidade;";

    // foreach($meses_ordenados as $mes => $values)
    // {
    //     foreach($value as $produto => $quantidade)
    //     {
    //         $sql = "INSERT INTO lista_de_produtos (mes, categoria, produto, quantidade)VALUES (:mes, :categoria, :produto, :quantidade)";
    //         $smtp = $pdo->prepare($sql);
            
    //         if(is_null($palavras_certas[$produto]))
    //         {
    //             $smtp->execute([":mes" => $mes, ":categoria" => $categoria, ":produto" => $produto, ":quantidade" => $quantidade]);
    //         }
    //         else
    //         {
    //             $smtp->execute([":mes" => $mes, ":categoria" => $categoria, ":produto" => $palavras_certas[$produto], ":quantidade" => $quantidade]);
    //         }
    //     }
        
    // }

    foreach($meses_ordenados as $mes => $values)
    {
        $alimentos = $values['alimentos'];
        $higiene_pessoal = $values['higiene_pessoal'];
        $limpeza = $values['limpeza'];

        if(!empty($alimentos))
        {
            arsort($alimentos);
            
            foreach($alimentos as $produto => $quantidade)
            {
                $sql = "INSERT INTO lista_de_produtos (mes, categoria, produto, quantidade)VALUES (:mes, :categoria, :produto, :quantidade)";
                $smtp = $pdo->prepare($sql);
                if(!in_array($alimentos, $palavras_certas))
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'alimentos', ":produto" => $produto, ":quantidade" => $quantidade]);
                }
                else
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'alimentos', ":produto" => $palavras_certas[$produto], ":quantidade" => $quantidade]);
                }
            }
        }
        if(!empty($higiene_pessoal))
        {
            arsort($higiene_pessoal);

            foreach($higiene_pessoal as $produto => $quantidade)
            {
                if(!in_array($produto, $palavras_certas))
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'hgiene pessoal', ":produto" => $produto, ":quantidade" => $quantidade]);
                }
                else
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'higiene pessoal', ":produto" => $palavras_certas[$produto], ":quantidade" => $quantidade]);
                }
            }
        }
        if(!empty($limpeza))
        {
            arsort($limpeza);

            foreach($limpeza as $produto => $quantidade)
            {
                if(!in_array($produto, $palavras_certas))
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'limpeza', ":produto" => $produto, ":quantidade" => $quantidade]);
                }
                else
                {
                    $smtp->execute([":mes" => $mes, ":categoria" => 'limpeza', ":produto" => $palavras_certas[$produto], ":quantidade" => $quantidade]);
                }
            }
        }
    }