<?php

    function getsortedMonth($monthList = [])
        {
            $meses = array(
                "janeiro" => "january",
                "fevereiro" => "february",
                "marco"     => "march",
                "abril" => "april",
                "maio" => "may",
                "junho" => "june",
                "julho" => "july",
                "agosto" => "august",
                "setembro" => "september",
                "outubro" => "october",
                "novembro" => "november",
                "dezembro" => "december"
            );
            $new_sort = [];

            foreach($monthList as $month => $val)
            {
                $m = date_parse($meses[$month]);
                $output[$m['month']] = $month;
            }
            ksort($output);
            
            foreach($output as $key)
            {
                $new_sort[$key] = $monthList[$key];
            }

            return $new_sort;
        }